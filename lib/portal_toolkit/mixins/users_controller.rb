module PortalToolkit::Mixins
  module UsersController
      # before_action :authenticate_user!
      # before_action :authorize_admin!

      def index
        @q = User.ransack(params[:q])
        @users = @q.result(distinct: true)
      end

      def new
        @user = User.new
      end

      def show
        @user = User.find(params[:id])
      end

      def edit
        @user = User.find(params[:id])
      end

      def create
        @user = User.new(user_params)

        if @user.save
          redirect_to action: :show, id: @user
        else
          redirect_to action: :new
        end
      end

      def update
        @user = User.find(params[:id])

        strip_blank_password!

        if @user.update(user_params)
          redirect_to action: :show, id: @user
        else
          redirect_to action: :edit
        end
      end

      def destroy
        @user = User.find(params[:id])

        if @user.destroy
          redirect_to action: :index
        else
          redirect_to action: :show, id: @user
        end
      end

      def reset_password
        @user = User.find(params[:id])
        @user.send_reset_password_instructions
        flash[:notice] = "Sent password reset instructions to #{@user.email}"
        redirect_to action: :show, id: @user
      end

      private

      def user_params
        params.require(:user).permit(:first_name, :last_name, :email, :password,
           :password_confirmation, :is_admin)
      end

      def strip_blank_password!
        if params[:user][:password].blank?
          params[:user].delete(:password)
          params[:user].delete(:password_confirmation)
        end
      end
  end
end