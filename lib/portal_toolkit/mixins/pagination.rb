module PortalToolkit::Mixins
	module Pagination
    def limit
      limit = sanitized_params.fetch(:limit, max_per_page).to_i
      [limit, max_per_page].min
    end

    def max_per_page
      50
    end

    def current_page
      sanitized_params[:page] || 1
    end
	end
end