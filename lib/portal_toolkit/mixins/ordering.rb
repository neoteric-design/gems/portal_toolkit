module PortalToolkit::Mixins
  module Ordering
    def apply_order(collection, order_field, order_direction)
      return collection if order_field == :relevance
      collection.reorder(order_field => order_direction)
    end

    def orderable_fields
      raise NotImplementedError
    end

    def default_order_field
      raise NotImplementedError
    end

    def order_field
      field = params.dig(:order, :field).to_s.to_sym
      orderable_fields.include?(field) ? field : default_order_field
    end

    def order_direction
      direction = params.dig(:order, :direction).to_s.to_sym
      %i[asc desc].include?(direction) ? direction : :asc
    end
  end
end