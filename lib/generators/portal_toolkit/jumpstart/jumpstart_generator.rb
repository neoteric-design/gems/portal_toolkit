class PortalToolkit::JumpstartGenerator < Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)

  def add_gems
    gsub_file "Gemfile", /^.*webpacker.*$/, ""

    gem_group :development, :test do
      gem "bundler-audit", ">= 0.5.0", require: false
      gem "brakeman"

      gem "rspec-rails"
      gem "bullet"
      gem "launchy"
      gem "pry-rails"
    end

    gem_group :test do
      gem "factory_bot_rails"
      gem "faker"
      gem "simplecov", require: false
    end

    gem_group :production do
      gem "ohmysmtp-rails"
      gem "rack-canonical-host"
      gem "rack-timeout"
      gem "rails_12factor"
      gem "raygun4ruby"
    end

    gem "ransack"
    gem "devise"
    gem "view_component", require: "view_component/engine"
    gem "simple_form"
    gem "hiredis"
    gem "dry-monads"
    gem "pagy"
    gem "stripe"
    gem "money-rails"
    gem "active_link_to"
    gem "webpacker", "~> 6.0.0.rc.5"

    Bundler.with_unbundled_env do
      run "bundle install"
    end
  end

  def apply_default_application_config
    application do
      <<~CONFIG
        config.generators do |generate|
          generate.helper false
          generate.javascripts false
          generate.request_specs false
          generate.routing_specs false
          generate.stylesheets false
          generate.test_framework :rspec
          generate.view_specs false
          generate.system_tests = true
        end
      CONFIG
    end

    gsub_file "config/application.rb",
              /# config.time_zone.*$/,
              %{config.time_zone = "Central Time (US & Canada)"}

    copy_file "puma.rb", "config/puma.rb"
  end

  def apply_default_development_config
    gsub_file "config/environments/development.rb",
              %(# config.i18n.raise_on_missing_translations = true),
              %(config.i18n.raise_on_missing_translations = true)

    application nil, env: "development" do
      <<~CONFIG
        config.after_initialize do
          Bullet.enable = true
          Bullet.console = true
          Bullet.rails_logger = true
        end
      CONFIG
    end
  end

  def apply_default_production_config
    application nil, env: "production" do
      <<~CONFIG
        config.action_mailer.default_url_options = { host: ENV["APPLICATION_HOST"] }
        config.action_mailer.delivery_method = :ohmysmtp
        config.action_mailer.ohmysmtp_settings = {
          api_token: ENV["OHMYSMTP_API_TOKEN"]
        }

        if ENV["APPLICATION_HOST"].present?
          config.middleware.use Rack::CanonicalHost,
            ENV["APPLICATION_HOST"],
            cache_control: "max-age=3600"
        end

        if ENV["REDIS_URL"]
          config.cache_store = :redis_cache_store, { url: ENV['REDIS_URL'] }
        end
      CONFIG
    end

    gsub_file "config/environments/production.rb",
              /# config.force_ssl.*$/,
              %(config.force_ssl = true)

    gsub_file "config/environments/production.rb",
              /config.log_level.*$/,
              %(config.log_level = ENV.fetch("RAILS_LOG_LEVEL", :info))
  end

  def install_ci
    copy_file "gitlab-ci.yml", ".gitlab-ci.yml"
    gsub_file ".gitlab-ci.yml", "RUBY_VERSION", RUBY_VERSION
    copy_file "database.yml.gitlab", "config/database.yml.gitlab"
  end

  def configure_dev_tools
    copy_file "rubocop.yml", ".rubocop.yml"
    remove_file ".ruby-version"
    create_file ".tool-versions" do
      "ruby #{RUBY_VERSION}"
    end
  end

  def install_dev_helper_tasks
    copy_file "dev.rake", "lib/tasks/dev.rake"
  end

  def add_default_i18n
    copy_file "en.yml", "config/locales/en.yml"
  end

  def configure_non_production_mailers
    copy_file "mail_interceptor.rb", "config/initializers/mail_interceptor.rb"

    application nil, env: "development" do
      %(  config.action_mailer.default_url_options = {host: "localhost:3000"})
    end

    application nil, env: "test" do
      %(  config.action_mailer.default_url_options = {host: "www.example.com"})
    end
  end

  def configure_misc_gems
    generate "simple_form:install"

    initializer "pagy.rb" do
      "require 'pagy'"
    end

    inject_into_module "app/helpers/application_helper.rb", "ApplicationHelper" do
      "  include Pagy::Frontend\n"
    end
  end

  def configure_rspec
    generate "rspec:install"
    copy_file "rspec/spec_helper.rb", "spec/spec_helper.rb"

    insert_into_file "spec/rails_helper.rb",
                     before: "RSpec.configure do |config|\n" do
      <<~CONFIG

      require "rspec/rails"
      require "pry-rails"

      Dir[Rails.root.join("spec/_support/**/*.rb")].sort.each { |file| require file }

      CONFIG
    end

    insert_into_file "spec/rails_helper.rb",
                     after: "RSpec.configure do |config|\n" do
      <<~CONFIG

          config.include FactoryBot::Syntax::Methods
          config.include Devise::Test::ControllerHelpers, type: :controller
          config.include Warden::Test::Helpers, type: :system
          config.include Warden::Test::Helpers, type: :request
          config.include Devise::Test::IntegrationHelpers, type: :system
          config.include Devise::Test::IntegrationHelpers, type: :request
          config.include Capybara::RSpecMatchers, type: :helper
          config.include ViewComponent::TestHelpers, type: :component
          config.include Rails.application.routes.url_helpers, type: :component

          config.include StripeFormHelper, type: :system
      CONFIG
    end
  end

  def add_rspec_support_files
    create_file "spec/factories.rb" do
      <<~CONFIG
        FactoryBot.define do

        end
      CONFIG
    end

    %w[capybara.rb i18n.rb stripe_form_helper.rb stripe_tokens.rb].each do |fn|
      copy_file "rspec/#{fn}", "spec/_support/#{fn}"
    end
  end

  def add_ignores
    append_to_file ".gitignore", "\ncoverage/"
  end

  def setup_webpacker
    run "bin/rails webpacker:install"
  end

  def add_setup_deploy_scripts
    copy_file "setup", "bin/setup"
    copy_file "deploy", "bin/deploy"
  end

  def add_procfile
    copy_file "Procfile", "Procfile"
  end

  def add_formatting_helpers
    copy_file "formatting_helper.rb", "app/helpers/formatting_helper.rb"
    copy_file "time_formats.rb", "config/initializers/time_formats.rb"
  end
end
