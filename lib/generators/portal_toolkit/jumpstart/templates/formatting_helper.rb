module FormattingHelper
  def format_money(money)
    humanized_money_with_symbol money, no_cents_if_whole: false
  end

  def format_date(date)
    return unless date
    date.to_date.to_formatted_s(:simple_dmy)
  end

  def format_datetime(time)
    return unless time
    time.to_formatted_s(:simple_dmy)
  end
end

