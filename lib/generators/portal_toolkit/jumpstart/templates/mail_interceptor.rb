if ENV['REDIRECT_MAIL_TO']
  # Don't send real emails in staging
  class StagingEmailInterceptor
    def self.delivering_email(message)
      message.subject.prepend "Intecepted Message for #{message.to} -- "
      message.to = ENV['REDIRECT_MAIL_TO']
    end
  end

  ActionMailer::Base.register_interceptor(StagingEmailInterceptor)
end
