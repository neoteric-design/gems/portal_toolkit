module StripeFormHelper
  def stripe_card_number
    "4242424242424242"
  end

  def stripe_card_cvc
    "123"
  end

  def stripe_card_exp
    next_year = 1.year.from_now.year % 100

    "09#{next_year}"
  end

  def stripe_card_zip
    "60618"
  end

  def fill_in_stripe_element(selector = "body", number: stripe_card_number,
    cvc: stripe_card_cvc,
    exp: stripe_card_exp,
    zip: stripe_card_zip)

    wait_for_stripe do
      within selector do
        within_frame 0 do
          slow_send_chars("cardnumber", number)
          slow_send_chars("cvc", cvc)
          slow_send_chars("exp-date", exp)
          slow_send_chars("postal", zip)
        end
      end
    end
  end

  def wait_for_stripe(&block)
    using_wait_time(20) { yield }
  end

  private

  def slow_send_chars(field, string)
    string.chars.each do |char|
      find_field(field).send_keys(char)
      sleep 0.1
    end
  end
end
