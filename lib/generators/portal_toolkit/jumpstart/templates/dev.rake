if Rails.env.development? || Rails.env.test?
  namespace :dev do
    desc "Sample data for local development environment"
    task admin_user: :environment do
      User.create(email: "admin@example.com", password: "password", is_admin: true)
    end
  end

  namespace :heroku do
    namespace :db do
      desc "Drop local database and replace with remote data"
      task :pull, [:remote] do |_t, args|
        remote = args.fetch(:remote, "production")
        dev_database = Rails.configuration.database_configuration.dig("development", "database")
        exit unless dev_database.present?
        Rake::Task["db:drop"].invoke
        system("heroku pg:pull DATABASE #{dev_database} -r #{remote}")
        Rake::Task["db:create"].invoke
        Rake::Task["db:migrate"].invoke
      end
    end

    namespace :s3 do
      desc "Download remote assets to local public folder"
      task :pull, [:remote] do |_t, args|
        remote = args.fetch(:remote, "production")
        bucket_name = `heroku config:get S3_BUCKET -r #{remote}`.strip
        puts "Copying system assets from #{bucket_name} to local public folder.."
        system "aws s3 cp s3://#{bucket_name}/system ./public/system --recursive"
      end
    end

    desc "Clone both remote database and S3 assets locally"
    task :clone, [:remote] do |_t, args|
      remote = args.fetch(:remote, "production")
      Rake::Task["heroku:db:pull"].invoke(remote)
      Rake::Task["heroku:s3:pull"].invoke(remote)
    end
  end
end
