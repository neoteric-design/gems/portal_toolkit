class PortalToolkit::ResourceGenerator < Rails::Generators::NamedBase
  source_root File.expand_path('templates', __dir__)
  class_option :fields, type: :array, default: [], desc: "attribute args for model"

  def create_model
    generate "model #{name} #{options[:fields].join(" ")}"
  end

  def create_admin_ui
    inject_into_file "config/routes.rb", after: "namespace :admin do\n" do
      "    resources :#{table_name}\n"
    end

    inject_into_file "app/views/layouts/admin.html.erb", after: /<% content_for :nav_items do %>\n/ do
      %(<%= link_to "#{plural_name.titleize}", admin_#{table_name}_path %>)
    end

    template "admin_controller.rb.erb", "app/controllers/admin/#{table_name}_controller.rb"
    template "_form.html.erb", "app/views/admin/#{table_name}/_form.html.erb"
    template "_partial.html.erb", "app/views/admin/#{table_name}/_#{singular_name}.html.erb"
    template "index.html.erb", "app/views/admin/#{table_name}/index.html.erb"
    template "show.html.erb", "app/views/admin/#{table_name}/show.html.erb"
    template "edit.html.erb", "app/views/admin/#{table_name}/edit.html.erb"
    template "new.html.erb", "app/views/admin/#{table_name}/new.html.erb"
  end

  private

  def field_names
    options[:fields].map(&:to_s)
                    .map{ |f| f.split(":") }
                    .map(&:first)
  end
end
