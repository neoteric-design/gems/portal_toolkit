module Admin
  class ApplicationController < ::ApplicationController
    layout "admin"

    include Pagy::Backend

    before_action :authenticate_user!
    before_action :authorize_admin!

    def authorize_admin!
      redirect_to root_path,
                  alert: t("defaults.not_authorized") unless current_user.is_admin
    end

    def show
      find_resource
    end

    def new
      build_resource
    end

    def edit
      find_resource
    end
2
    def create
      resource = build_resource

      if update_resource
        flash[:notice] = t(".success", default: t("defaults.create.success", model: resource.class))
        redirect_to action: :show, id: resource.id
      else
        flash[:alert] =  t(".error", default: t("defaults.create.error", model: resource.class))
        render :new, status: :unprocessable_entity
      end
    end

    def update
      resource = find_resource

      if update_resource
        flash[:notice] = t(".success", default: t("defaults.update.success", model: resource.class))
        redirect_to action: :show, id: resource.id
      else
        flash[:alert] =  t(".error", default: t("defaults.update.error", model: resource.class))
        render :edit, status: :unprocessable_entity
      end
    end

    def destroy
      resource = find_resource

      if destroy_resource
        flash[:notice] = t(".success", default: t("defaults.destroy.success", model: resource.class))
        redirect_to action: :index
      else
        flash[:alert] = t(".error", default: t("defaults.destroy.error", model: resource.class))
        redirect_to action: :show, id: resource.id, status: :unprocessable_entity
      end
    end

    def permitted_params
      raise NotImplementedError
    end

    def show_attributes
      raise NotImplementedError
    end

    def build_resource
      raise NotImplementedError
    end

    def find_resource
      raise NotImplementedError
    end

    def update_resource
      raise NotImplementedError
    end

    def destroy_resource
      raise NotImplementedError
    end

    private

    def default_sort
      "created_at desc"
    end

    def set_default_sort
      params[:q] ||= {}
      params[:q][:s] ||= default_sort
    end
  end
end
