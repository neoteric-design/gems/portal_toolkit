module ErrorHandling
  extend ActiveSupport::Concern

  included do
    rescue_from "StandardError" do |exception|
      render_server_error(exception: exception)
    end

    rescue_from "ActionView::MissingTemplate",
                "ActiveRecord::RecordNotFound",
                "ActionController::UnknownFormat",
                "ActionController::RoutingError" do
      render_not_found
    end
  end

  def render_server_error(exception:)
    @exception = exception
    log_error(exception)
    report_error(exception)
    render "static/server_error", layout: "application", status: :internal_server_error
  end

  def render_not_found
    render "static/not_found", layout: "application", status: :not_found
  end

  def log_error(e)
    logger.error e.message
    logger.error e.backtrace.join("\n")
  end

  def report_error(e)
    Raygun.track_exception(e, request.env) if defined? Raygun
  end
end
