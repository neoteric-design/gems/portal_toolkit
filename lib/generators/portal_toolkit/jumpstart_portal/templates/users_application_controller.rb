module Users
  class ApplicationController < ::ApplicationController
    layout "users"
    before_action :authenticate_user!

    include Pagy::Backend

    def permitted_params
      raise NotImplementedError
    end

    def scoped_collection
      raise NotImplementedError
    end

    def find_resource
      scoped_collection.find(params[:id])
    end
  end
end