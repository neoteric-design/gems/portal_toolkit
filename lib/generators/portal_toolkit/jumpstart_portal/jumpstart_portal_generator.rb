class PortalToolkit::JumpstartPortalGenerator < Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)

  def configure_devise
    generate "devise:install"

    gsub_file "config/initializers/devise.rb", "config.sign_out_via = :delete", "config.sign_out_via = :get"

    generate "devise User"
    directory "devise", "app/views/devise"
    migration = Dir.entries("db/migrate").detect { |fn| fn =~ /create_users/ }

    desired_fields = %w[sign_in_count current_sign_in_at last_sign_in_at current_sign_in_ip last_sign_in_ip]

    desired_fields.each do |field|
      uncomment_lines "db/migrate/#{migration}", /#{field}/
    end

    insert_into_file "db/migrate/#{migration}", after: /create_table.*$/ do
     %(\n      t.boolean :is_admin, default: false, index: true\n)
    end

    gsub_file "app/models/user.rb", ":validatable", ":validatable, :trackable"
  end

  def add_app_namespaces
    generate "controller", "admin/dashboards index --no-assets --skip-routes -t rspec"
    generate "controller", "users/dashboards index --no-assets --skip-routes -t rspec"

    copy_file "users_application_controller.rb", "app/controllers/users/application_controller.rb"
    copy_file "admin_application_controller.rb", "app/controllers/admin/application_controller.rb"

    gsub_file "app/controllers/users/dashboards_controller.rb", "ApplicationController", "Users::ApplicationController"
    gsub_file "app/controllers/admin/dashboards_controller.rb", "ApplicationController", "Admin::ApplicationController"

    route "root to: 'dashboards#index'", namespace: :admin
    route "root to: 'dashboards#index'", namespace: :users
  end

  def add_static_pages
    generate "controller", "static contact privacy terms home --no-assets --no-helper --no-controller-specs --skip-routes"
    route 'get "contact", to: "static#contact", as: "contact"'
    route 'get "privacy-policy", to: "static#privacy", as: "privacy_policy"'
    route 'get "terms-of-service", to: "static#terms", as: "terms_of_service"'
    route 'root to: "static#home"'
  end

  def add_error_handling
    copy_file "error_handling.rb", "app/controllers/concerns/error_handling.rb"
    copy_file "not_found.html.erb", "app/views/templates/static/not_found.html.erb"
    copy_file "server_error.html.erb", "app/views/templates/static/server_error.html.erb"

    inject_into_class "app/controllers/application_controller.rb", "ApplicationController" do
      "  include ErrorHandling\n"
    end
  end

  def organize_layouts
    copy_file "base.html.erb", "app/views/layouts/base.html.erb"
    copy_file "application.html.erb", "app/views/layouts/application.html.erb"
    copy_file "application.html.erb", "app/views/layouts/users.html.erb"
    copy_file "application.html.erb", "app/views/layouts/admin.html.erb"

    empty_directory "app/views/shared"
    copy_file "_header.html.erb", "app/views/shared/_header.html.erb"
    copy_file "_footer.html.erb", "app/views/shared/_footer.html.erb"
    copy_file "_flashes.html.erb", "app/views/shared/_flashes.html.erb"
    copy_file "_sidebar.html.erb", "app/views/shared/_sidebar.html.erb"
  end

  def add_user_crud
    template "admin_controller.rb.erb", "app/controllers/admin/users_controller.rb"
    directory "users", "app/views/admin/users"

    inject_into_file "config/routes.rb", after: "namespace :admin do\n" do
      "    resources :users\n"
    end

    inject_into_file "app/views/layouts/admin.html.erb", after: /<% content_for :nav_items do %>\n/ do
      %(<%= link_to "Users", admin_users_path %>)
    end
  end
end
