class PortalToolkit::JumpstartTailwindGenerator < Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)


  def add_yarn_packages
    needed_packages = %w[
      css-loader css-minimizer-webpack-plugin
      mini-css-extract-plugin sass sass-loader postcss postcss-loader
      postcss-flexbugs-fixes postcss-import postcss-preset-env tailwindcss @tailwindcss/forms
      @tailwindcss/aspect-ratio
    ]

    `yarn add #{needed_packages.join(" ")}`
  end

  def add_to_postcss
    copy_file "postcss.config.js", "postcss.config.js"
    inject_into_file "postcss.config.js", after: "plugins: [" do
      "\n    require('tailwindcss'),\n"
    end
  end

  def generate_config
    `npx tailwindcss init`
  end

  def add_tailwind_plugins
    inject_into_file "tailwind.config.js", after: "plugins: [" do
      <<~STR

          require('@tailwindcss/forms'),
          require('@tailwindcss/aspect-ratio'),

      STR
    end
  end

  def add_app_stylesheet
    copy_file "application.scss", "app/packs/entrypoints/application.scss"
  end
end
