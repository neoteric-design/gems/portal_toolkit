class PortalToolkit::JumpstartApiGenerator < Rails::Generators::Base
  source_root File.expand_path('templates', __dir__)

  def fix_inflection
    append_to_file "config/initializers/inflections.rb" do
      <<~CODE
      ActiveSupport::Inflector.inflections(:en) do |inflect|
        inflect.acronym 'API'
      end
      CODE
    end
  end

  def add_base_controller
    copy_file "api_controller.rb", "app/controllers/api/v1/application_controller.rb"

    inject_into_file "config/routes.rb", after: "Rails.application.routes.draw do\n" do
      <<~ROUTES
          namespace :api, defaults: { format: :json }, constraints: { format: :json } do
            namespace :v1 do

            end
          end
      ROUTES
    end
  end

  def setup_serializer
    gem "blueprinter"
    gem "oj"

    Bundler.with_original_env do
      run "bundle install"
    end

    copy_file "blueprinter.rb", "config/initializers/blueprinter.rb"
  end
end
