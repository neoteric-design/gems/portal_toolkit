module API::V1
  class ApplicationController < ActionController::API
    rescue_from "StandardError", with: :render_generic_error
    respond_to :json

    def render_error(message:, code:, status: :internal_server_error)
      render status: status, json: { message: message, code: code }
    end

    def render_not_found
      render_error message: I18n.t("general.errors.record_not_found"),
                   code: "record_not_found",
                   status: :not_found
    end

    def render_generic_error
      render_error message: I18n.t("general.errors.generic_error"),
                   code: "generic_error"
    end
  end
end
