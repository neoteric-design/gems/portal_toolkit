# ⚠️ Archival Notice ⚠️ 

This project was a part of Neoteric Design's internal tooling and prototypes. 

These are no longer officially supported or maintained, and may contain bugs or be in any stage of (in)completeness.

This repository is provided as a courtesy to our former clients to support their projects going forward, as well in the interest of giving back what we have to the community. If you found yourself here, we hope you find it useful in some capacity ❤️ 


--------------------

# PortalToolkit

Scripts and tools to build and support Neoteric style portal projects.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'portal_toolkit'
```

## New project setup

This gem provides several tools to help jumpstart new portal projects

### Rails 6.1

```bash
rails _6.1.4.1_ new APPNAME --skip-webpack-install --skip-turbolinks -d postgresql
```

### Rails 7

```bash
# TBD
```

`bin/rails generate portal_toolkit:jumpstart`

Takes a bare Rails project and applies various configs, adds our commonly used gems, sets up CI, testing, and other things we universally use for Rails projects.

```bash
bin/rails generate portal_toolkit:jumpstart_tailwind
```

Sets adds and sets up TailwindCSS through webpacker

```bash
bin/rails generate portal_toolkit:jumpstart_portal
```

Applies a basic portal application skeleton. Admin/User/Public namespaces and layouts.

```bash
bin/rails generate portal_toolkit:jumpstart_api
```

Applies the basic groundwork for a JSON API

## Resource Generation

Generate a model, along with an admin namespace CRUD UI for it

```bash
bin/rails generate portal_toolkit:resource MODEL_NAME --fields attribute_one attribute_two:integer
```
