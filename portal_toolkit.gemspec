$:.push File.expand_path("lib", __dir__)

# Maintain your gem"s version:
require "portal_toolkit/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "portal_toolkit"
  spec.version     = PortalToolkit::VERSION
  spec.authors     = ["Madeline Cowie"]
  spec.email       = ["madeline@cowie.me"]
  spec.homepage    = "https://www.neotericdesign.com"
  spec.summary     = "Tools and scripts to support building Neoteric style portal applications"
  spec.description = "Tools and scripts to support building Neoteric style portal applications"
  spec.license     = "Propietary"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the "allowed_push_host"
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", ">= 6.1"
  spec.add_dependency "devise"
  spec.add_dependency "ransack"
  spec.add_dependency "view_component"

  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "capybara"
  spec.add_development_dependency "factory_bot_rails"
  spec.add_development_dependency "faker"
  spec.add_development_dependency "rspec-rails", ">= 3.7.0"
  spec.add_development_dependency "bundle-audit"
  spec.add_development_dependency "brakeman"
  spec.add_development_dependency "selenium-webdriver"
  spec.add_development_dependency "simplecov"
  spec.add_development_dependency "pry-rails"
  spec.add_development_dependency "webpacker"
end
