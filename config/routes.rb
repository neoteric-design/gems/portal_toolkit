PortalToolkit::Engine.routes.draw do
  root to: "pages#home"

  scope "profile", controller: "profile" do
    get "edit"
    post "update"
  end

  namespace :admin do
    resources :users do
      member do
        get "reset_password"
      end
    end
  end
end
