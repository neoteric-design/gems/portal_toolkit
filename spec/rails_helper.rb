ENV["RACK_ENV"] = "test"

require File.expand_path("../dummy/config/environment", __FILE__)
abort("DATABASE_URL environment variable is set") if ENV["DATABASE_URL"]
abort("The Rails is running in production mode!") if Rails.env.production?

require "rspec/rails"
require "pry-rails"
require "factory_bot_rails"


Dir["#{__dir__}/support/**/*.rb"].sort.each { |file| require file }

RSpec.configure do |config|
  config.include FactoryBot::Syntax::Methods
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.include Devise::Test::ControllerHelpers, type: :controller
  config.include Warden::Test::Helpers, type: :system
  config.include Warden::Test::Helpers, type: :request
  config.include Devise::Test::IntegrationHelpers, type: :system
  config.include Devise::Test::IntegrationHelpers, type: :request
  config.include Capybara::RSpecMatchers, type: :helper

  config.include RequestHelpers, type: :request
  config.include StripeFormHelper, type: :system

  config.use_transactional_fixtures = true
  config.infer_base_class_for_anonymous_controllers = false
  config.infer_spec_type_from_file_location!
end

ActiveJob::Base.queue_adapter = :test
ActiveRecord::Migration.maintain_test_schema!
