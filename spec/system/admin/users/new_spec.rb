require "rails_helper"

RSpec.describe "Admin - Users - New", type: :system, js: true do
  let(:admin) { create :user, :admin }
  let(:user) { create :user }
  before(:each) { sign_in admin }

  it "requires admin" do
    sign_out admin
    visit portal_toolkit.new_admin_user_path
    expect(current_path).to eq(portal_toolkit.new_user_session_path)
  end

  it "renders form" do
    visit portal_toolkit.new_admin_user_path

    expect(page).to have_field("First name")
    expect(page).to have_field("Last name")
    expect(page).to have_field("Email")
  end


  it "creates record" do
    visit portal_toolkit.new_admin_user_path

    attrs = {
      "First name" => Faker::Name.first_name,
      "Last name" => Faker::Name.last_name,
      "Email" => Faker::Internet.email,
      "Password" => Faker::Internet.password
    }

    attrs.each do |field, value|
      fill_in field, with: value
    end
    fill_in "Password confirmation", with: attrs["Password"]

    click_on "Create User"

    attrs.except("Password").each do |key, value|
      expect(page).to have_content(value)
    end
  end
end