require "rails_helper"

RSpec.describe "Admin - Users - Show", type: :system do
  let(:admin) { create :user, :admin }
  let(:user) { create :user }
  before(:each) { sign_in admin }

  it "requires admin" do
    sign_out admin
    visit portal_toolkit.admin_user_path(user)
    expect(current_path).to eq(portal_toolkit.new_user_session_path)
  end

  it "display user attributes" do
    visit portal_toolkit.admin_user_path(user)

    expect(page).to have_text(user.name)
    expect(page).to have_text(user.email)
  end

  it "displays Public Activity" do

  end

  xit "displays user roles" do
    property = create(:property, :with_address)
    user.grant(:admin)
    user.grant(:client, property)


    visit portal_toolkit.admin_user_path(user)

    expect(page).to have_text("admin")
    expect(page).to have_text("client")
  expect(page).to have_text(property.address.to_s)
  end
end