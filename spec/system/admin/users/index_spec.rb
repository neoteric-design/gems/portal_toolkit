require "rails_helper"

RSpec.describe "Admin - Users - Index", type: :system do
  let(:admin) { create :user, :admin }
  it "requires admin" do
    visit portal_toolkit.admin_users_path
    expect(current_path).to eq(portal_toolkit.new_user_session_path)
  end

  it "lists users" do
    users = create_list(:user, 3)
    sign_in admin

    visit portal_toolkit.admin_users_path

    users.each do |user|
      expect(page).to have_text(user.name)
      expect(page).to have_text(user.email)
    end
  end
end