

# Admin
User.create(first_name: "Jerry", last_name: "Admin", is_admin: true, email: "servers@neotericdesign.com", password: "neoteric")

# Normal users
User.create(first_name: "Melissa", last_name: "User", email: "melissa@example.com", password: "neoteric")
User.create(first_name: "Kevin", last_name: "User", email: "kevin@example.com", password: "neoteric")