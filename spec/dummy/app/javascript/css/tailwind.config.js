module.exports = {
  purge: [
    './static/**/*.html',
    './static/**/*.js',
    './layouts/**/*.html',
    './layouts/*.html',
    './_vendor/**/**/*.html'
  ],
  theme: {
    extend: {
      colors: {
      }
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/ui')
  ],
}
