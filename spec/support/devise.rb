module SessionHelper
  def manual_sign_in(email = "user@example.com", password = "password")
    fill_in "Email", with: email
    fill_in "Password", with: password
    click_button "Sign In"
  end
end
