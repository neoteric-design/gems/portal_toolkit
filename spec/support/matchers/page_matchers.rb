RSpec::Matchers.define :have_flash do |level, text: nil|
  match do |session|
    session.has_selector?(".flash.flash-#{level}", text: text, visible: :all)
  end
end