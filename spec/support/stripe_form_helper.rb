module StripeFormHelper
  def stripe_card_number
    "4242424242424242"
  end

  def stripe_card_cvc
    "123"
  end

  def stripe_card_exp
    next_year = 1.year.from_now.year % 100

    "09#{next_year}"
  end

  def stripe_card_zip
    "60618"
  end

  def fill_in_stripe_element(selector, number: stripe_card_number,
    cvc: stripe_card_cvc,
    exp: stripe_card_exp,
    zip: stripe_card_zip)
    using_wait_time(5) do
      within selector do
        within_frame 0 do
          number.chars.each do |digit|
            find_field("cardnumber").send_keys(digit)
            sleep 0.2
          end
          find_field("cvc").set(cvc)
          find_field("exp-date").set(exp)
          find_field("postal").set(zip)
        end
      end
    end
  end

  def wait_for_stripe(&block)
    using_wait_time(20) { yield }
  end
end
