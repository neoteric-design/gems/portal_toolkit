if defined?(OmniAuth)
  OmniAuth.config.test_mode = true

  OmniAuth.config.add_mock(:facebook, {
    provider: "facebook",
    uid: "12345",
    info: {
      email: "test@test.com",
      name: "Facebook Tester",
      image: "tester.jpg",
    },
  })

  OmniAuth.config.add_mock(:twitter, {
    provider: "twitter",
    uid: "12345",
    info: {
      email: "test@test.com",
      name: "Twitter Tester",
      image: "tester.jpg",
    },
  })

  OmniAuth.config.add_mock(:google_oauth2, {
    provider: "google_oauth2",
    uid: "12345",
    info: {
      email: "test@test.com",
      name: "Google Tester",
      image: "tester.jpg",
    },
  })
end
