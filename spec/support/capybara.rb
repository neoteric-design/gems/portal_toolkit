Capybara.server = :webrick
Capybara.server_host = "0.0.0.0"

RSpec.configure do |config|
  config.before(:each, type: :system) do
    driven_by :rack_test
  end

  if ENV["SELENIUM_URL"].present?
    config.before(:each, type: :system, js: true) do
      ip = Socket.ip_address_list.detect { |addr| addr.ipv4_private? }.ip_address
      host! "http://#{ip}:#{Capybara.server_port}"

      driven_by :selenium, using: :chrome, screen_size: [1400, 1400],
                options: {url: ENV["SELENIUM_URL"]}
    end
  else
    config.before(:each, type: :system, js: true) do
      driven_by :selenium, using: :headless_chrome, screen_size: [1400, 1400]
    end
  end
end
