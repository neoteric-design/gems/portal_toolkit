require "faker"

FactoryBot.define do
  factory :user, class: PortalToolkit::User do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    email { Faker::Internet.email }
    password { 'password' }

    trait(:admin) do
      is_admin { true }
    end
  end
end
