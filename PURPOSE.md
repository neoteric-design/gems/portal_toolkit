# Goals
The goal of Portal Toolkit is to accelerate portal project development and maintenance. To achieve this, identify where we spend time and what can be either centralized or automated


# Philosophy

- Avoid trying to anticipate every need of a project
- Prefer (first) delivering smaller slices of functionality to build with, over fully-formed large features
- Prefer generators & templates to add features to a project, over centralized/"tucked away" in the gem. Especially for things that often need to be customized (e.g controllers)


# Features

- Project Setup
	- Apply our commonly used configurations and libraries
	- Apply our portal app skeleton, including front end layouts
	- Heroku servers, addons, env vars
	- AWS resources (S3, IAM keys)

- Basic Portal Functions
	- User accounts
	- Admin privileges
	- Data access and filtering (sorting, pagination, search, permissions, etc)
	- Consistent but flexible UX, including rapid CRUD interfaces
	- Modules to aid more specific features (e.g file uploads)


## Data Access and Filtering

- ransack for on the fly sorting and search
- pagy for pagination
- (?) Pundit for permission mgmt
- Generators to drop in a new resource CRUD, as close to fully functional as feasible, with tests


## Consistent but flexible UX
Componentized and layered front end. Be intentional about the level of abstraction during development. Generic as possible building blocks, and build specificity atop. Allows for ease of use at the top, but still exposing the smaller pieces to remix and reuse as needed. 

- ViewComponent for rendering 
- TailwindCSS for visual styling
- Hotwire (stimulus and turbo) for interactivity

Tailwind and Stimulus each bring a similar philosophy of building functionality out of small composable pieces. ViewComponents can easily make use of one or many stimulus controllers for the various needed behaviors, and can hold a default set of Tailwind styles without worrying about the cascade. Very complimentary set.


### Resource/Top Layer:

"Display a resource index table of FileUpload records with the columns and actions we want by default, maybe turn off a column that doesn't make sense on this particular view"

- Preconfigured lower level component, geared towards a specific use case or model
- Batteries included, should be one-line drop-in and go
- Minimal amount of options, if something too different is needed, reach down a layer and create your own


### Middle Layer:

"Display a resource index table, given these records, columns, actions and batch actions"

- Similar to base layer, but more complex
- Likely incorporates some base layer components
- Still generic, not tied to a specific model, but may be geared towards a specific use


### Base Layer:

"Display a little tag bubble that may or may not be linked"

- General purpose
- Atomic
- (Ideally) Highly reusable in different contexts



