# frozen_string_literal: true

class PortalToolkit::ResourceTableComponent < ViewComponent::Base
  renders_one :head
  renders_many :rows
  attr_reader :css_classes

  def initialize(classes: {})
    @css_classes = classes.with_defaults(default_css_classes)
  end

  def default_css_classes
    {
      table: "min-w-full divide-y divide-gray-200",
      thead: "px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider",
      th: nil,
      tr: "tableRow bg-gray-100",
      td: "px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900",
      standard_action: "text-teal-600 hover:text-teal-900 mr-3",
      destructive_action: "text-red-600 hover:text-red-900 mr-3"
    }
  end

  def column_header(label, **attributes)
    attributes[:class] ||= @css_classes[:th]
    content_tag :th, label, attributes
  end

  def cell(content = nil, **attributes, &block)
    attributes[:class] ||= @css_classes[:td]
    content_tag :td, content, **attributes, &block
  end

  def action_link(label, href, **attributes)
    attributes[:class] ||= @css_classes[:standard_action]
    link_to label, href, attributes
  end

  def delete_link(label = "Delete", href, **attributes)
    attributes[:class] ||= @css_classes[:destructive_action]
    attributes[:method] ||= :delete
    attributes[:data] ||= {}
    attributes[:data][:confirm] ||= "Are you certain you want to delete this?"

    link_to label, href, attributes
  end
end
