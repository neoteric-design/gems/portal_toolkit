# frozen_string_literal: true

class PortalToolkit::ResourceDetailsTableComponent < ViewComponent::Base

  attr_reader :css_classes
  renders_many :rows, -> (*args) {
    attribute_row(*args)
  }

  def initialize(resource, classes: {})
    @resource = resource
    @css_classes = classes.with_defaults(default_css_classes)
  end

  def label_cell(contents = nil, **options)
    options.with_defaults!(class: @css_classes[:value_cell])
    content_tag :td, contents, options
  end

  def value_cell(contents = nil, **options)
    options.with_defaults!(class: @css_classes[:value_cell])
    content_tag :td, contents, options
  end

  def attribute_row(attribute_name, label: nil, value: nil, row_options: {}, label_options: {}, value_options: {})
    label ||= attribute_name.to_s.humanize
    value ||= @resource.public_send(attribute_name)
    row_options.with_defaults!(class: @css_classes[:tr])

    content_tag :tr, row_options do
      concat label_cell label, label_options
      concat value_cell value, value_options
    end
  end


  def default_css_classes
    {
      table: "min-w-full divide-y divide-gray-200",
      thead: "px-6 py-3 bg-gray-50 text-left text-xs leading-4 font-medium text-gray-500 uppercase tracking-wider",
      tbody: nil,
      tr: "tableRow bg-gray-100",
      header_cell: "px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900",
      label_cell: "px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900",
      value_cell: "px-6 py-4 whitespace-no-wrap text-sm leading-5 font-medium text-gray-900"
    }
  end
end
