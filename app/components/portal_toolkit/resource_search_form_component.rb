# frozen_string_literal: true

class PortalToolkit::ResourceSearchFormComponent < ViewComponent::Base
  include Ransack::Helpers::FormHelper
  class SearchFormField
    attr_reader :name, :label

    def initialize(name, label: nil)
      @name = name
      @label = label || name.to_s.humanize
    end
  end

  renders_many :fields, "SearchFormField"
  attr_reader :css_classes

  def initialize(q:, classes: {})
    @query = q
    @css_classes = classes.with_defaults(default_css_classes)
  end

  def default_css_classes
    {
      search_field: "block w-full h-full pl-8 pr-3 py-2 rounded-md text-sm text-cool-gray-900 placeholder-cool-gray-500 focus:outline-none focus:placeholder-cool-gray-400 sm:text-sm",
      submit: "mr-0 md:mx-3 float-right inline-flex items-center px-4 py-2 border border-teal-600 text-sm leading-5 font-medium rounded-md text-teal-600 bg-transparent hover:text-white hover:border-transparent hover:bg-teal-500 focus:outline-none focus:border-teal-700 focus:shadow-outline-indigo active:bg-teal-700 transition ease-in-out duration-150"
    }
  end
end
