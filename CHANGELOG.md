# CHANGELOG

## 0.2.1

* Fix rake build skipping files

## 0.2.0

* Bugfixes
* Branding work
* Setup/deploy script

## 0.1.1

* Bugfixes

## 0.1.0

* Initial publishing, incomplete
